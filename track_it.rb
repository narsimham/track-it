#!/usr/bin/env ruby

require 'yaml'

config = YAML::load_file('config/config.yml')

def prompt(q)
  print q['prompt'] + ' '
end

def get(q)
  answer = nil
  while answer == nil
    prompt(q)
    input = gets.chomp.strip

    next if input == ''

    case q['type']
      when 'boolean'
        case input.upcase
          when 'Y', 'YES'
            answer = true
          when 'N', 'NO'
            answer = false
        end
      when 'integer'
        if input.to_i.to_s == input
          answer = input.to_i
        end
    end
  end

  answer
end

config['questions'].each do |q|
  q['answer'] = get(q)
end

now = Time.now
output_filename_base = 'data/' + now.strftime('%Y-%m-%d')
output_filename_index = 0

output_filename = output_filename_base + '.txt'
while File.exist?(output_filename)
  output_filename_index += 1
  output_filename = output_filename_base + '-' + output_filename_index.to_s + '.txt'
end

File.open(output_filename, 'w') do |f|
  YAML::dump(config['questions'], f)
end
